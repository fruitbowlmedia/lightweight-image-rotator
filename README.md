#Simple, Lightweight Rotator#

To make this code snippet work, you will need the following HTML and CSS style;


```
#!HTML

<div id="rotator">
 <ul class="axle" style="width: 100%;">
  <li><img src="../img_url1.jpg"></li>
  <li><img src="../img_url2.jpg"></li>
  <li><img src="../img_url3.jpg"></li>
  ...
 </ul>
</div>
```


```
#!CSS

#rotator {
 white-space: no-wrap !important;
 overflow: hidden;
}
ul.axle {
 padding: 0;
 list-style: none;
 display: block;
 width: 99999px; /* Script will auto calculate, this value is a fallback safe value so the user doesn't see shifting elements */
}
ul.axle > li {
 display: block;
 float: left;
 width: 120px; /* Equal to child width + gutter (set in Script)
}
ul.axle > li > * {
 max-width: 100px;
}
```

One the framework is in place, all you need to do is include the following and you're all set!

** Don't forget to include jQuery and Easing.js (Optional; Change 'easeOutExpo' to 'linear' if you exclude easing.js) **


```
#!jQuery
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script>
 var $child, $axle, $rotator, $childWidth, $childCount, $margin, $animTime, $delay;
		
 $animTime = 1200; //animation length
 $delay = $animTime + 1400; // time between animations. Can't be less than $animTime
		
 $rotator = '#rotator'; //selector for rotator container
 $axle = $($rotator+' ul.axle'); //axle, usuallu <ul>
 $child = $($rotator+' ul.axle li');
 $childFirst = $rotator+' ul.axle li:nth-of-type(1)'; // the elements that will be sliding
		
 $gutter = 20; //gutter width between children elements
 $childCount = $axle.children().length; // Counts child elements
		
 $child.css({'padding-right': $margin + 'px'});
 $childWidth = $child.outerWidth();
 $axle.css({'width': ($childWidth * $childCount)});
		
 setInterval(function() {
  $axle.animate({
   marginLeft: '-=' + $childWidth
  }, $animTime, 'easeOutExpo', function() {
   $axle.css({
    marginLeft: '0'
   });
   var $first = $($childFirst).appendTo($axle);
  });
 }, $delay);
</script>
```